import 'package:flutter/material.dart';
import 'authentication.dart';
import 'package:toast/toast.dart';

class LoginRegister extends StatefulWidget {
  LoginRegister({this.auth, this.onSignedIn});
  final AuthImplementation auth;
  final VoidCallback onSignedIn;

  @override
  _LoginRegisterState createState() => _LoginRegisterState();
}

enum FormType { login, register }

class _LoginRegisterState extends State<LoginRegister> {
  final formKey = new GlobalKey<FormState>();
  FormType _formType = FormType.login;
  String _email = "";
  String _password = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Insta"),
        ),
        resizeToAvoidBottomPadding: false,
        body: Container(
          margin: EdgeInsets.all(15.0),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: createInputs() + createButtons(),
            ),
          ),
        ));
  }

  List<Widget> createInputs() {
    return [
      SizedBox(
        height: 10.0,
      ),
      logo(),
      SizedBox(
        height: 20.0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Email'),
        validator: (value) {
          return value.isEmpty ? 'Email is requiered.' : null;
        },
        onSaved: (value) {
          return _email = value;
        },
      ),
      SizedBox(
        height: 10.0,
      ),
      TextFormField(
        decoration: InputDecoration(labelText: 'Password'),
        obscureText: true,
        validator: (value) {
          return value.isEmpty ? 'Password is requiered.' : null;
        },
        onSaved: (value) {
          return _password = value;
        },
      ),
      SizedBox(
        height: 20.0,
      ),
    ];
  }

  Widget logo() {
    return Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 110.0,
        child: Image.asset("images/insta.png"),
      ),
    );
  }

  List<Widget> createButtons() {
    if (_formType == FormType.login) {
      return [
        FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            "Login",
            style: TextStyle(fontSize: 20.0),
          ),
          textColor: Colors.white,
          color: Colors.purple,
          onPressed: validateAndSubmit,
        ),
        FlatButton(
          child: Text(
            "Not have an Account? Create Account?",
            style: TextStyle(fontSize: 15.0),
          ),
          textColor: Colors.red,
          onPressed: moveToRegister,
        ),
      ];
    } else {
      return [
        FlatButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            "Create Account",
            style: TextStyle(fontSize: 20.0),
          ),
          textColor: Colors.white,
          color: Colors.purple,
          onPressed: validateAndSubmit,
        ),
        FlatButton(
          child: Text(
            "Already have an Account? Login",
            style: TextStyle(fontSize: 15.0),
          ),
          textColor: Colors.red,
          onPressed: moveToLogin,
        )
      ];
    }
  }

//Declaramos var y enum
  bool validateAndSave() {
    final form = formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    } else {
      return false;
    }
  }

  void validateAndSubmit() async {
    if (validateAndSave()) {
      try {
        if (_formType == FormType.login) {
          String userId = await widget.auth.signIn(_email, _password);
          Toast.show("Welcome! ", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        } else {
          String userId = await widget.auth.signUp(_email, _password);
          Toast.show("New user ", context,
              duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
        }
        widget.onSignedIn();
      } catch (e) {
        Toast.show("check your email or password ", context,
            duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
      }
    }
  }

  void moveToRegister() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.register;
    });
  }

  void moveToLogin() {
    formKey.currentState.reset();
    setState(() {
      _formType = FormType.login;
    });
  }
}
