import 'package:flutter/material.dart';
import 'mapping.dart';
import 'authentication.dart';

void main() => runApp(BlogApp());

class BlogApp extends StatelessWidget {
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Insta",
      theme: ThemeData(primarySwatch: Colors.orange),
      debugShowCheckedModeBanner: false,
      home: MappingPage(
        auth: Auth(),
      ),
    );
  }
}
